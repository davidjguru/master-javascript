$(document).ready(function(){
  console.log("The Regular Expressions file has been loaded");

  // Build Posts List
       var posts = [
         {
           name: 'Exercise 1',
           title: 'Regular Expressions: Match a Literal String with Different Possibilities',
           link: 'https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/regular-expressions/match-a-literal-string-with-different-possibilities',
           date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
           content: 'Using regexes like /coding/, you can look for the pattern "coding" in another string. This is powerful to search single strings, but it\'\ s limited to only one pattern. You can search for multiple patterns using the alternation or OR operator: |. This operator matches patterns either before or after it. For example, if you wanted to match "yes" or "no", the regex you want is /yes|no/. You can also search for more than just two patterns. You can do this by adding more patterns with more OR operators separating them, like /yes|no|maybe/.',
           exercise: 'Complete the regex petRegex to match the pets "dog", "cat", "bird", or "fish"',
           code: ["let petString = 'James has a pet cat.'",
           "let petRegex = /dog|cat|bird|fish/; // Change this line",
           "let result = petRegex.test(petString);"]
          },
          {
            name: 'Exercise 2',
            title: 'Regular Expressions: Restrict Possible Usernames',
            link: 'https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/regular-expressions/restrict-possible-usernames',
            date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
            content: 'Usernames are used everywhere on the internet. They are what give users a unique identity on their favorite sites. You need to check all the usernames in a database. Here are some simple rules that users have to follow when creating their username.' +
            '1) The only numbers in the username have to be at the end. There can be zero or more of them at the end.' +
            '2) Username letters can be lowercase and uppercase.' +
            '3) Usernames have to be at least two characters long. A two-letter username can only use alphabet letter characters',
            exercise: 'Change the regex userCheck to fit the constraints listed above.',
            code: ["let username = 'JackOfAllTrades';",
            "let userCheck = //i; // Change this line",
            "let result = userCheck.test(username);"]
           },
       ];

       console.log(posts);
       posts.forEach((item, index) => {
          var post =
          `<article class="post">
            <h2>${item.name}</h2>
            <h3>${item.title}</h3>
            <p>${item.link}</p>
            <br>
            <span class="date">${item.date}</span>
            <p>${item.content}</p>
            <p>${item.exercise}</p>
            <code>${item.code[0]}</code><br>
            <code>${item.code[1]}</code><br>
            <code>${item.code[2]}</code><br>
          </article>
          <hr />`;
          console.log(post);
          $("#paragraphs").append(post);
       });


    // Calling to functions

      function specifyRanges(chain){
        let ohStr = chain;
        let ohRegex = /Oh{3,6}\sno/i; // Change this line
        let result = ohRegex.test(ohStr);
        return "Specify Upper and Lower Number of Matches: " + ohStr + ": "  + result;
      }

      function restrictName(name){
        console.log("Executing restrictName() function");
        let username = name;
        let userCheck = /^[a-z]{2,}\d*$/gi; // Change this line
        let result = userCheck.test(username);
        return "restricName by RegExp: " + username + ": "  + result;
        }

        function normalAndWhitespace(chain){
          let sample = chain;
          let countNonWhiteSpace = /\S/g; // Change this line
          let result = sample.match(countNonWhiteSpace);
          return "Match non-whitespace characters: " + sample + ": "  + result;
        }

    // Calling specifyRanges() function
    console.log(specifyRanges("Ohhh no"));
    console.log(specifyRanges("Ohhh no"));
    console.log(specifyRanges("Ohhhhhh no"));
    console.log(specifyRanges("Ohhhhhhh no"));
    console.log(specifyRanges("Ooooooooh no"));
    console.log(specifyRanges("Oooooooohh no"));

    // Calling restrictName() function
     console.log(restrictName("JackOfAllTrades"));
     console.log(restrictName("JACK"));
     console.log(restrictName("007JamesBond"));
     console.log(restrictName("Ocean11"));

    // Calling normalAndWhitespace() function
    console.log(normalAndWhitespace("Whitespace is important in separating words"));
    console.log(normalAndWhitespace("123 ddeeeee BGH M"));
});
