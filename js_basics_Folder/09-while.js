"use strict"

// Loop While

var year = 2018;

while (year <= 2051) {
  // Execute this piece of code
  console.log("We are now at year: "+year);
  year++;
}
console.log("Finally, we are at 2051");



while (year != 1991) {
  // Execute this piece of code
  console.log("We are now at year: "+year);
if (year == 2000) {
  break;
}

  year--;
}
console.log("Finally, we are at year: "+year);

// Loop Do - While

var years = 30;
do {
  alert("Only when years is not equal to 20");
  years--;
} while (years > 25);
