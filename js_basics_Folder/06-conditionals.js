"use strict"

// Conditional if
// If A == B then do something

var age1 = 10;
var age2 = 12;

// If this is true
if(age1 > age2){
  // Execute This
  console.log("Age 1 es bigger than Age 2");
} else{
  console.log("Age 1 is smaller than Age 2");
}

/* Relationals Operators are:
   Bigger than: >
   Smaller than: <
   Bigger or equal: >=
   Smaller or equal: <=
   Equal: ==
   Unequal: !=
*/

// Another example
var age = 34;
var name = "John Doe";

if (age >= 18){
console.log(name+" has "+ age);

  if(age <= 33){
    console.log("You are a millenial yet");
  }else if (age >= 70) {
    console.log("You are a old person");
  }else{
    console.log("Ok, you aren't a millenial");
  }

}else {
  console.log(name+" has "+age+", It's under 18");
}

/* Logical Operators
AND(Y): &&
OR (O): ||
NOT (No): !
*/
var year = 2028;

// Negation Operator
if(year != 2016){
  console.log("Year It's not equal");
}

// AND Operator
if (year >= 2000 && year <= 2020 && year != 2018){
  console.log("We are in the current era");
}else{
  console.log("We are in full post-modern age, dude");
}

// OR Operator
if (year == 2008 || (year >= 2018 && year == 2028)){
  console.log("The year ends in eight");
}else{
  console.log("Year not registered");
}
