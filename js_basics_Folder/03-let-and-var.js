'use strict'
// Testing with let and var
// alert("Hello");

// Using var
var number = 40;
console.log(number); // value = 40

if (true){
var number = 50;
console.log(number); // value = 50
}

console.log(number); // value = 50


// Using let
var text = "Javascript Course"; // It's a global value
console.log(text); // value JS

if(true){
let text = "Angular Training"; // This value will works only within the block
console.log(text); // value Angular
}

console.log(text); // value JS
