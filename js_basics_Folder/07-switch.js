"use strict"

// Switch

var age = 40;
var printing = "";
switch (age) {
  case 18:
    printing = "You are 18 years old";
    console.log("Age is: "+age);
    break;
  case 25:
    printing = "You are 25 years old";
    console.log("Age is: "+age);
    break;
  case 40:
    printing = "You are 40 years old";
    console.log("Age is: "+age);
    break;
  default:
    printing = "You're out of the cases";
    console.log("Age is: "+age);
}
console.log(printing);
