"use strict"
alert("Hey, alert load");
// Operators
// Arithmetics Operators - Basics
var number1 = 7;
var number2 = 12;
var operation1 = number1 + number2;
var operation2 = number1 - number2;
var operation3 = number1 * number2;
var operation4 = number1 / number2;
var operation5 = number1 % number2;

//Showing Results
alert("The result 1 is: "+operation1);
alert("The result 2 is: "+operation2);
alert("The result 3 is: "+operation3);
alert("The result 4 is: "+operation4);
alert("The result 5 is: "+operation5);
console.log("The result 1 is: "+operation1);
console.log("The result 2 is: "+operation2);
console.log("The result 3 is: "+operation3);
console.log("The result 4 is: "+operation4);
console.log("The result 5 is: "+operation5);



// Data types - Basics
// Int, Float, String, Boolean, Array, Objects
var int_var = 44;
var float_var = 0.346;
var string_var = "Hello 'what is going on' dude?";
var boolean_var = true;

console.log("Int var is: "+int_var);
console.log("Float var is: "+float_var);
console.log("String var is: "+string_var);
console.log("Boolean var is: "+boolean_var);

// Testing
var false_int = "33";
var true_int = 8;
console.log(false_int+true_int);
console.log(Number(false_int)+true_int);
console.log(String(true_int)+false_int);

// Typeof
console.log(typeof false_int);
console.log(typeof true_int);
console.log(typeof boolean_var);
