'use strict'

// Scope with var or let 
let test = "Hello";


// Working with variables
// A variable is only a container to save information

var country = "France";
var continent = "Europe";
var age = 2000;
// The next line will give an error in console, by use strict
population = 1000000;

var country_and_continent = country+' '+continent;
var country_and_continent_and_age = country_and_continent+' '+age;

console.log(country, continent, age);
console.log(country_and_continent);
alert(country_and_continent_and_age);


country = "Canada";
continent = "America"
console.log(country);
var country_and_continent = country+' '+continent;
console.log(country_and_continent);
