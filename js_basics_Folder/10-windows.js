"use strict"

// Alert
alert("New alert");
alert("Another alert");

// Confirm
var my_result = confirm("Are you sure you want to continue?");
// Values from confirm: true or false
console.log(my_result);

// Data Input Window
var my_input = prompt("How old are you?", 18);
console.log(my_input);
console.log(typeof my_input);
// Remember: prompt always works with string format ;-) 
var my_parsed_input = parseInt(my_input);
console.log("My string has been parsed: "+my_parsed_input);
console.log(typeof my_parsed_input);
