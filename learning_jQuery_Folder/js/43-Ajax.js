$(document).ready(function(){
  console.log("Ajax.js has been loaded");
  var data1 = $("#data1");
  var data2 = $("#data2");
  var data3 = $("#data3");

  // Load
  console.log(data1);
  // Initial example - basic load, all HTML code
  // Obtains a 200 ok message with a GET Request
  // You can see this in "Network"

  data1.load("https://reqres.in/");

  // GET Method
   $.get("https://reqres.in/api/users", {page: 3}, function(response){
     // console.log(response);
     response.data.forEach((element, index) => {
       data2.append("<p>" + "Name: " + element.first_name + " - Surname: " +
                     element.last_name + "</p>");
     });
   } );

  // POST Method
  var user1 = {
    "name": "davidjguru",
    "job": "developer"
  };
  $.post("https://reqres.in/api/users", user1, function(response){
        console.log(response);
        var myJSON = JSON.stringify(response);
        var infoResponse = JSON.parse(myJSON);
        data3.append("<p>Name: " + infoResponse.name + "</p><br />Job: " +
                      infoResponse.job);
  });

   // Making the POST Request in a dinamic way throught a webform
   $("#formPOST").submit(function(e){
     e.preventDefault(); // Deny the behat by default, doesn't load the URL
     var user2 = {
         name: $('input[name="name"]').val(),
         job: $('input[name="job"]').val()
     };
     console.log(user2);
     $.post($(this).attr("action"), user2, function(response){
       console.log(response);
       var infoString = JSON.parse(JSON.stringify(response));
       data3.append("<p>New Name: " + infoString.name + "</p><br />New Job: " +
                     infoString.job);
     }).done(function(){
        alert("New User has been added correctly");
     });
     return false;
   });

   // Using $.Ajax
   var user3 = {
       name: 'Michael',
       job: "Knight Rider"
   };

   $.ajax({
     type: 'POST',
     url: 'https://reqres.in/api/users',
     dataType: 'json',
     data: user3,
     beforeSend: function(){
       console.log("Triying create a new user number three by POST in $.ajax");
       console.log(user3);
     },
     success: function(response){
       console.log(response);
       var infoResponse = JSON.parse(JSON.stringify(response));
       data3.append("<p>New Name3: " + infoResponse.name + "</p><br />New      Job3: " + infoResponse.job);
     },
     error: function(){
       console.log("An error occurred.");
     },
      timeout: 2000
   });

});
