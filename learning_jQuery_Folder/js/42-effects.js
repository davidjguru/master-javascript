$(document).ready(function(){
// First load test
console.log("Effects .js has been loaded");

// We'll using the hide and show methods of jQuery for our Box
  var box = $("#box");
  var hide = $("#hide_button");
  var show = $("#show_button");
  var anim = $("#animate");
  // Hidden box by default
      box.hide();
      hide.hide();
  // Then, Let's catch the mouse click for show
  show.click(function(){
   //box.show('slow');
   //box.fadeIn('slow');
   box.fadeTo('slow', 0.8);
   show.hide();
   //show.fadeOut('fast');
   hide.slideToggle('slow');

  });

  // And now we'll capture the mouse click for hide
  hide.click(function(){
    box.hide('fast');
    hide.hide();
    show.show(2000);
  });

  // Takin element div "animate"
  // .animate( properties [, duration ] [, easing ] [, complete ] )
    anim.click(function(){
    box.animate({marginLeft: '500px',
    opacity: 0.25,
    left: "+=50",
    height: "toggle",
    fontSize: '40px'
  }, 5000)
        .animate({
          fontSize: '10px',
          marginTop: '300px',
          borderRadius: '20px',
        }, 3000)
        .animate({
          fontSize: '25px',
          marginTop: '0px',
          marginLeft: '20px',
          opacity: 1
        }, 2000);

    });
});
