$(document).ready(function(){
  // alert("Events.js has been loaded");
  console.log("Events file has been loaded");

  // MouseOver and MouseOut

  var box = $("#box");
  var data = $("#data");
/*
  box.mouseover(function(){
     $(this).css("background", "blue");
  });
  box.mouseout(function(){
     $(this).css("background", "green");
  });
  */

  function redBackground(){
     $(this).css("background", "blue");
  }

  function greenBackground(){
     $(this).css("background", "green");
  }


  // Hover
  box.hover(redBackground, greenBackground);

  // Click and Double Click
  box.click(function(){
     $(this).css("background", "yellow")
            .css("color", "white");
  });
  box.dblclick(function(){
     $(this).css("background", "pink")
            .css("color", "red");
  });

   // Focus and Blur
   var name = $("#name");
  name.focus(function(){
    $(this).css("border", "10px solid green");
  });

  name.blur(function(){
    $(this).css("border", "1px solid #ccc");
    data.text($(this).val()).show();
  });

  // Mouse Down and Mouse Up
    data.mousedown(function(){
      $(this).css("border-color", "gray ");
    });
    data.mouseup(function(){
      $(this).css("border-color", "red ");
    });

    // Mousemove
      $(document).mousemove(function(){
        $('body').css("cursor", "none");
        var followMe = $("#follow_me");
        console.log("In X: "+event.clientX);
        console.log("In Y: "+event.clientY);
        followMe.css("left", event.clientX)
                .css("top", event.clientY);
      });
});
