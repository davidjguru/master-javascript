// $ is always referring to jQuery
// $ == JQuery, is an object
// The element in brackets it's the selector (all the document in this case)
// At last, .ready is a call to an event about whole load of the document.
  $(document).ready(function(){
    console.log("jQuery and your basic web example with HTML charged...");
    // Id selectors example
    var red = $("#red");
    console.log(red);
    $("#red").css("background", "red")
             .css("color", "white");

    var yellow = $("#yellow").css("background", "yellow")
                             .css("color", "green");
    console.log(yellow);
    var green = $("#green").css("background", "green")
                           .css("color", "yellow");
    console.log(green);

    // Class selectors example
     var myclass = $('.phrases');
     console.log(myclass);
     // Classical extraction as an Array
     console.log("First element of the myclass Arrray: ", myclass[0]);
     // jQuery extraction way, allows use methods
     console.log(myclass.eq(0));
     console.log(myclass.eq(1));
     console.log(myclass.eq(2));
     //myclass.css("border", "5px dashed black");
     myclass = $('.phrases').css("padding", "20px");
     $('.no_border').click(function(){
       console.log("Clicked element: ");
       $(this).addClass('phrases');
       // Refresh index.html and look how the style doesn't change
     });

     // Now, we're testing selectors by tags
     var paragraphs = $('p').css('cursor', 'pointer');
     paragraphs.click(function(){
       var thisvar = $(this);
       if((thisvar.hasClass('phrases')) && !((thisvar.hasClass('big')))) {
       thisvar.addClass("big");
     }
     else if ((thisvar.hasClass('phrases')) && ((thisvar.hasClass('big')))) {
      thisvar.removeClass('big');
     }
     });

     // Selectors by Attribute
     $('[title="Google"]').css('background', '#ccc');
     $('[title="Facebook"]').css('background', 'blue');

     // Find and Parents
     // Put a margin-top for paragraphs and links
     $('p, a').addClass('margin-top');

     // Looking for elements
     // Using with the find method:
     var searching = $('#box').find('.overwrite');
     console.log(searching);
     // It's useful to search in very large HTML trees
     var searching2 = $('#box .overwrite').eq(0).parent().parent().parent().find('[title="Twitter"]');
     console.log(searching2);
  });
