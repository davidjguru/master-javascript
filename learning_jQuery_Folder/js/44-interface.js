$(document).ready(function(){
  console.log("Ok, interface.js has been loaded");

  // Using Draggable
  $(".element").draggable();
  console.log("Now you can drag and drop the boxes in screen");

  // Using Resizable
  $(".element").resizable();
  console.log("And now you can resize the boxes");

  // Using Selectable
  // $(".selectable-list").selectable();
  console.log("Please, select a item within the list");

  // Using Sortable
  // Well, but you can only use sortable or selectable at same time.
  // So we turn off the previous call to selectable.
  $(".selectable-list").sortable({
    update: function(event, ui){
      console.log("There has been a change");
    }
  });

   // Using Droppable
   $("#moving-element").draggable();
   $("#area").droppable({
     drop: function(){
       console.log("You have moved an item within the area");
     }
   });

   // Using Effects
   var box = $(".box-effect");
   $("#show").click(function(){
     box.toggle("fade", 4000);
     box.effect("explode", 2000);
     box.toggle("blind", 3000 );
     box.toggle("slide", 2000);
     box.toggle("drop", 4000);
     box.toggle("fold", 3000);
     box.toggle("puff", 2000);
     box.toggle("scale", 4000);
     box.toggle("shake", 3000);
   });

   // Using Tooltips
   // (Tooltip replaces native tooltips)
   $(document).tooltip({
     classes: { "ui-tooltip": "highlight" },
     show: { effect: "explode", duration: 1000 },
     track: true
   });


   // Using Dialog Box
   $("#launch-popup").click(function(){
   $("#popup").dialog();
   console.log("The Dialog Box has been loaded");
   });


   // Using Calendar as Data Picker
   $("#datepicker").datepicker();

   // Using Tabs
   $("#my-tabs").tabs({active: 0});

});
