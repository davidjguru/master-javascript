
var datevar = new Date();
// Show the current date: Day, Month, Year, hh:mm:ss
console.log(datevar);
var year = datevar.getYear();
var month = datevar.getMonth();
var day = datevar.getDay();
var fullyear = datevar.getFullYear();
var hour = datevar.getHours();
// See the differences between year and fullyear
console.log("Full Year: ", fullyear);
console.log("Year: ", year);
// The month is counted from zero ;-)
console.log("Month: ", month);
console.log("Day: ", day);
var dayDate = datevar.getDate();
console.log("Date full: ", dayDate);

// Now with interpolation :-)
var textHour = `
   The year is: ${fullyear}
   And the Month is: ${month}
   Finally, the day is: ${dayDate} .
   Hour: ${hour}
`;
console.log(textHour);

// Some of Mathematics
console.log(Math.random()*10000);
console.log(Math.ceil(Math.random()*1000000));
