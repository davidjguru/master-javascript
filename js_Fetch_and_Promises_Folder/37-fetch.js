"use strict";

window.addEventListener('load', function(){

// Testing the load with a simple alert window
// alert("Ciau");

// Let's Test a external API Rest connection
var users = [];
var books = [];
var div_users = document.querySelector("#users");
console.log(div_users);
var div_persons = document.querySelector("#persons");
console.log(div_persons);
var div_person = document.querySelector("#person");
console.log(div_person);


// Fetch (ajax)
fetch('https://jsonplaceholder.typicode.com/users')
// First: We capture the data and convert it into a json object
.then(data => data.json())
// Second: We have all the collected info in variable
.then(data => {
  users = data;
  console.log(users);
  users.map((user, i) =>{
    let name = document.createElement('h2');
    name.innerHTML = i + "- User Name: " + user.name + ", Nick: " + user.username;
    div_users.appendChild(name);
    document.querySelector(".load").style.display = 'none';
  });
});
// It's fine, now you can see the json objects with 10 users data in console.

// Test two: calling to another API REST
// But the response in console shows a first level of info called page
// Let's remove the first level of data received
// fetch('https://reqres.in/api/users?page=2')
getPersons()
.then(data2 => data2.json())
// And now processing the values getting the second level: data
.then((persons) => {
  listOfPersons(persons.data);

  return getPerson();
  })
  // Getting third div: Unique User (Person)
  .then(data3 => data3.json())
  .then((person) => {
   showUser(person.data);
   return getInfo();
   })
   .then(data4 => {
     console.log(data4);
   });

  function getPersons(){
    return fetch('https://reqres.in/api/users?page=2');
  }

  function getPerson(){
    return fetch('https://reqres.in/api/users/2');
  }

  function getInfo(){
    var professor = {
      name: "John",
      lastname: "Doe",
      url: 'https://fakedir.com'
    };
    return new Promise((resolve, reject) => {
    var professor_string = JSON.stringify(professor);
    if(typeof professor_string != 'string') return reject('error');
     return resolve(professor_string);
  });

}

  function listOfPersons(people){
    people.map((person, i) =>{
    let name = document.createElement('h2');
    name.innerHTML = i + "- ID Number: " + person.id + ", First Name: " + person.first_name + ", Last Name: " + person.last_name;
    div_persons.appendChild(name);
    document.querySelector(".load2").style.display = 'none';

    });
}

  function showUser(person){
    let name = document.createElement('h2');
    let avatar = document.createElement('img');
    name.innerHTML = "0" + "- ID Number: " + person.id + ", First Name: " + person.first_name + ", Last Name: " + person.last_name;
    avatar.src = "https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg";
    div_person.appendChild(name);
    div_person.appendChild(avatar);
    document.querySelector(".load3").style.display = 'none';
}


// Test three: Open Library REST API
// // But in this case, the response is not JSON Formatted
fetch('https://reststop.randomhouse.com/resources/authors?lastName=Grisham')
.then(data5 => {
  books = data5;
  console.log(books);
});

});
