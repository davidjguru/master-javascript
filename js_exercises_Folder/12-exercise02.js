"use strict"

/*
Using a loop, we'll show the sum value and an average value until you enter a
negative value, and then we'll give the result.
*/

var sum = 0;
var counter = 0;

do {
  var num = parseInt(prompt("Set the value until a negative number: ", 0));

  if (isNaN(num)){
    num = 0;
  }else if (num >= 0) {
    sum += num;
    counter++;
  }
  console.log(sum);
  console.log(counter);

} while (num >=0)

alert("The sum value is: "+sum);
alert("The average values is: "+ (sum/counter));
