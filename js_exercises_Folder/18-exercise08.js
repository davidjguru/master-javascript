"use strict"

/*
Calculator
- Ask for two numbers
- If the one of the number is wrong, back to ask the values
- Writing in the body page, in an alert and by prompt all the results
  from add, subtraction, multiplication and division.
*/


var number1 = parseInt(prompt("Set the first value: ", 0));
var number2 = parseInt(prompt("Set thesecond value: ", 0));


while(number1 < 0 || number2 < 0 || isNaN(number1) || isNaN(number2)){
  number1 = parseInt(prompt("Set the first value: ", 0));
  number2 = parseInt(prompt("Set thesecond value: ", 0));
}

var result = "The add is: "+(number1 + number2)+"<br>"+
             "The subtraction is: "+(number1 - number2)+"<br>"+
             "The multiplication is: "+(number1 * number2)+"<br>"+
             "The division is: "+(number1 / number2)+"<br>";

var resultCMD = "The add is: "+(number1 + number2)+"\n"+
                "The subtraction is: "+(number1 - number2)+"\n"+
                "The multiplication is: "+(number1 * number2)+"\n"+
                "The division is: "+(number1 / number2)+"\n";

document.write("<h2>"+result+"</h2>");
alert(resultCMD);
console.log(resultCMD);
