"use strict"

/*
Exercise: We will do a little program, asking for two numbers and return what
is greater than other, the smallest or if the numbers are equals.
*/

var number1 = parseInt(prompt("Set the first value", 0));
var number2 = parseInt(prompt("Set the second value", 0));
console.log ("Registered numbers by prompt are: " + "Number 1: "+number1+", "+
"Number 2: "+number2);

while((number1 <= 0) || (number2 <= 0) || (isNaN(number1)) ||
(isNaN(number2))){
  number1 = parseInt(prompt("Set the first value", 0));
  number2 = parseInt(prompt("Set the second value", 0));
}

if (number1 == number2){
  alert("The numbers are equals");
}else if (number1 > number2) {
  alert("The greatest number is: "+number1);
  alert("The smallest number is: "+number2);
}else if(number2 > number1){
  alert("The greatest number is: "+number2);
  alert("The smallest number is: "+number1);
}else{
  alert("Please, introduce right values");
}

// Extra: If the values aren't numbers or negatives values or zero
// Back to ask new values
