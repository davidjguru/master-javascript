"use strict"

// Show only the odd numbers between two values

var number1 = parseInt(prompt("Please, set the first value", 0));
var number2 = parseInt(prompt("And now the second value: ", 0));

while (number1 < number2) {
  number1++;
  if (number1%2 != 0){
    console.log("The number: "+number1+" is odd");
    }
}
