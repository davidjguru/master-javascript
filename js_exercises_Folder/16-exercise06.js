"use strict"

/*
Return if a number is odd or is even
1- Prompt Window
2- If the value is not valid, back to ask another number
*/

var number1 = parseInt(prompt("Set the number: ", 0));

while(isNaN(number1)){
  number1 = parseInt(prompt("Set the number: ", 0));
}

if (number1 % 2 == 0) {
  alert("It's an even number");

} else {
  alert("It's an odd number");

}
