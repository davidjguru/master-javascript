// This is the main file for my app
// import { Shirt } from './04-shirt';
var Main = /** @class */ (function () {
    function Main() {
        console.log("JS Application has been loaded");
    }
    Main.prototype.getShirt = function () {
        // return new Shirt("Yellow", "Big", "Adidas", "XXXL", 231);
    };
    return Main;
}());
var main = new Main();
console.log(main.getShirt());
console.table(main.getShirt());
