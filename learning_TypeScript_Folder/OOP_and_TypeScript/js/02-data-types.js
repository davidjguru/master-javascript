//  String
var word = "David";
console.log(word);
// Gets an error by type
// word = 12;
// Number
var age = 39;
// Boolean
var ugly = true;
// Gets another error by type
// ugly = 25;
// Any Allows using weak typing
var whatever = "Hello";
console.log(whatever);
// Doesn't get an error
whatever = 2345;
console.log(whatever);
// Arrays
var languages = ["PHP", "Java", "Javascript", "Python"];
console.log(languages);
// Gets an error from compiler 'cause the strong typing
// languages.push(12);
var valuesI18N = ["Spanish", 1, "English", 2, "Italian", 3];
console.log(valuesI18N);
var years = [1, 2, 4];
console.log(years);
var concepts = ["nine", 10, "eleven", 12, 13];
console.log(concepts);
// Gets an error from compiler 'cause the strong typing
// languages.push(12);
// Using Pipe
var charValue = "davidjguru@gmail.com";
console.log(charValue);
charValue = 9875;
console.log(charValue);
var exampleCustom = true;
console.log(exampleCustom);
exampleCustom = "Cacafuti";
console.log(exampleCustom);
// This gets an error in Transpiler (not in Console with Javascript)
// exampleCustom = 12430;
console.log(exampleCustom);
