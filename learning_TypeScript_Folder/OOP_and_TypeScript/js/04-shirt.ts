// Interface
 interface ShirtBase{
 setColor(color);
 getColor();
}

// Decorator
function printItem(logo: string){
return function(target: Function){
  target.prototype.printing = function():void{
    console.log("A Shirt printed with a logo", logo);
  }
}
}
// Classes
@printItem("Swoosh")
 class Shirt implements ShirtBase{
// Properties
   private color: string;
   public model: string;
   public brand: string;
   public size: string;
   public price: number;

// Methods
constructor(color, model, brand, size, price){
   this.color = color;
   this.model = model;
   this.brand = brand;
   this.size = size;
   this.price = price;
}

// Getters and Setters
    public setColor(colorInput){
      this.color = colorInput;
  }

  public getColor(){
    return this.color;
  }
}

// Inheritance
class Sweatshirt extends Shirt{

public hood: boolean;

setHood(hood: boolean){
  this.hood = hood;
  }

  getHood():boolean{
    return this.hood;
  }

}



// Breaking the contract defined in the Interface
var moreShirt = new Shirt("fdsa", "fdsa", "fdsa", "fdsa", 34 );
console.log("One more Shirt: ", moreShirt);
//   setColorOne(color),  getColorTwo() are breaking the contract


// Creating more instances of the class
var maglietta = new Shirt("", "", "", "", "");
console.log(maglietta);
maglietta.setColor("Red");
maglietta.model = "skinny";
maglietta.brand = "Levis";
maglietta.size = "XL";
maglietta.price = 75;


var tShirt = new Shirt("", "", "", "", "");
console.log(tShirt);
tShirt.setColor("Green");
tShirt.model = "Baggy";
tShirt.brand = "Lee";
tShirt.size = "M";
tShirt.price = 27;

// Creating instances of the children class
var myhood = new Sweatshirt("Red", "Old", "Nike", "XXL", 12);
console.log(myhood);
myhood.setHood(true);
myhood.setColor("Yellow");
console.log("Changed Hood: " ,myhood);
console.log(maglietta, tShirt);
maglietta.setColor("Blue");
console.log(maglietta.getColor());
