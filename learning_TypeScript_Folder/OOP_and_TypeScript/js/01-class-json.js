// This is only a way to create a class in Javascript
// In a traditional way, for old browsers.

// We're checking if the script is loaded
 console.log("Class Json.js");

// Defining a class

var bicycle = {
  // Properties
  color: 'Red',
  model: 'BMX',
  brakes: 'disc',
  MaximumSpeed: '60mph',
  // Methods
  changeColor: function(new_color){
    // bicycle.color = new_color;
    this.color = new_color;
    // console.log(this);
  }
};

console.log(bicycle);
bicycle.changeColor("Blue");
// Checking one more time the Bicycle object with the new color
console.log(bicycle);
