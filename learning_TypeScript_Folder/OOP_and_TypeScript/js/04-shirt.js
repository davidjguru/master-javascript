var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Decorator
function printItem(logo) {
    return function (target) {
        target.prototype.printing = function () {
            console.log("A Shirt printed with a logo", logo);
        };
    };
}
// Classes
var Shirt = /** @class */ (function () {
    // Methods
    function Shirt(color, model, brand, size, price) {
        this.color = color;
        this.model = model;
        this.brand = brand;
        this.size = size;
        this.price = price;
    }
    // Getters and Setters
    Shirt.prototype.setColor = function (colorInput) {
        this.color = colorInput;
    };
    Shirt.prototype.getColor = function () {
        return this.color;
    };
    Shirt = __decorate([
        printItem("Swoosh")
    ], Shirt);
    return Shirt;
}());
// Inheritance
var Sweatshirt = /** @class */ (function (_super) {
    __extends(Sweatshirt, _super);
    function Sweatshirt() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sweatshirt.prototype.setHood = function (hood) {
        this.hood = hood;
    };
    Sweatshirt.prototype.getHood = function () {
        return this.hood;
    };
    return Sweatshirt;
}(Shirt));
// Breaking the contract defined in the Interface
var moreShirt = new Shirt("fdsa", "fdsa", "fdsa", "fdsa", 34);
console.log("One more Shirt: ", moreShirt);
//   setColorOne(color),  getColorTwo() are breaking the contract
// Creating more instances of the class
var maglietta = new Shirt("", "", "", "", "");
console.log(maglietta);
maglietta.setColor("Red");
maglietta.model = "skinny";
maglietta.brand = "Levis";
maglietta.size = "XL";
maglietta.price = 75;
var tShirt = new Shirt("", "", "", "", "");
console.log(tShirt);
tShirt.setColor("Green");
tShirt.model = "Baggy";
tShirt.brand = "Lee";
tShirt.size = "M";
tShirt.price = 27;
// Creating instances of the children class
var myhood = new Sweatshirt("Red", "Old", "Nike", "XXL", 12);
console.log(myhood);
myhood.setHood(true);
myhood.setColor("Yellow");
console.log("Changed Hood: ", myhood);
console.log(maglietta, tShirt);
maglietta.setColor("Blue");
console.log(maglietta.getColor());
