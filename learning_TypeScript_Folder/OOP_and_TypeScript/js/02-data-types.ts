//  String
let word: string = "David";
console.log(word);

// Gets an error by type
// word = 12;

// Number
let age: number = 39;

// Boolean
let ugly: boolean = true;

// Gets another error by type
// ugly = 25;

// Any Allows using weak typing
let whatever: any = "Hello";
console.log(whatever);
// Doesn't get an error
whatever = 2345;
console.log(whatever);

// Arrays
var languages: Array<string> = ["PHP", "Java","Javascript","Python"];
console.log(languages);
// Gets an error from compiler 'cause the strong typing
// languages.push(12);
var valuesI18N: Array<any> = ["Spanish", 1, "English", 2, "Italian", 3];
console.log(valuesI18N);
let years: number[] = [1, 2, 4];
console.log(years);
let concepts: any[] = ["nine", 10, "eleven", 12, 13];
console.log(concepts);
// Gets an error from compiler 'cause the strong typing
// languages.push(12);


// Using Pipe
let charValue: string | number = "davidjguru@gmail.com";
console.log(charValue);
charValue = 9875;
console.log(charValue);

// Custom data types with alias
type lettersorboolean = string | boolean;
let exampleCustom: lettersorboolean = true;
console.log(exampleCustom);
exampleCustom = "Cacafuti";
console.log(exampleCustom);
// This gets an error in Transpiler (not in Console with Javascript)
// exampleCustom = 12430;
console.log(exampleCustom);
