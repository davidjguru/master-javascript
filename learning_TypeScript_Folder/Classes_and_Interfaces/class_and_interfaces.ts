interface Person{
     name: string;
     surname: string;
     age: number;

     getName(): string;
     throwGreeting(): void;
     setGreetingConsole(): void;
}

class User implements Person{
     name: string;
     surname: string;
     age: number;

   constructor(name: string, surname: string, age: number){
      this.name = name;
      this.surname = surname;
      this.age = age;
    }

     buildGreeting(){
       return "Hello "+this.name;
     }
      getName(){
        return this.name;
      }

      throwGreeting(){
       document.body.innerHTML = this.buildGreeting();
      }

      setGreetingConsole(){
       console.log(this.buildGreeting());
     }

}

function greetingHTML(name: string, surname: string, age: number){
  let person = new User(name, surname, age);
      person.throwGreeting();
      person.setGreetingConsole();
      console.log(person.getName());
}

greetingHTML("John", "Smith", 21);
