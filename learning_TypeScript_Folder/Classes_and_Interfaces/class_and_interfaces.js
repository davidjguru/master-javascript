var User = /** @class */ (function () {
    function User(name, surname, age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    User.prototype.buildGreeting = function () {
        return "Hello " + this.name;
    };
    User.prototype.getName = function () {
        return this.name;
    };
    User.prototype.throwGreeting = function () {
        document.body.innerHTML = this.buildGreeting();
    };
    User.prototype.setGreetingConsole = function () {
        console.log(this.buildGreeting());
    };
    return User;
}());
function greetingHTML(name, surname, age) {
    var person = new User(name, surname, age);
    person.throwGreeting();
    person.setGreetingConsole();
    console.log(person.getName());
}
greetingHTML("John", "Smith", 21);
