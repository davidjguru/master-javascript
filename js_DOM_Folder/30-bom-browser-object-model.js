"use strict"

// Browser Object Model
function getBom(){
console.log("This is the Height: " +window.innerHeight);
console.log("This is the Width: " +window.innerWidth);
console.log("This is the Screen Width: "+screen.width);
console.log("This is the Screen Height: "+screen.height);
}

function getURL(){
  console.log("URL location: "+window.location);
  console.log("Path and Filename: "+window.location.pathname);
}

function redirect(url){
  window.location.href = url;
}
function openWindow(url){
  window.open(url, "", "width=400, height=300");
}

getBom();
getURL();
// And try to call redirect() openWindow() from Console with values
