"use strict"

window.addEventListener('load', function(){
 console.log("Loaded DOM!");
 var myform = document.querySelector("#form");
 var box_dashed = document.querySelector(".dashed");
 box_dashed.style.display = "none";

     myform.addEventListener('submit', function(){
       console.log("Submit form");
       var name = document.querySelector("#name").value;
       var surname = document.querySelector("#surname").value;
       var age = parseInt(document.querySelector("#age").value);
       console.log(name, surname, age);

       // We're validating
       if(name.trim() == null || name.trim().length == 0){
         alert ("The name is not valid");
         return false;
       }
       if(surname.trim() == null || surname.trim().length == 0){
         alert ("The surname is not valid");
         return false;
       }
       if(age == null || isNaN(age) || age <= 0){
         alert ("The age is not valid");
         return false;
       }
       // var data_user = [name, surname, age];
       // var index;
       // var breakline = document.createElement("hr");
       // for(index in data_user){
       //   var paragraph = document.createElement("p");
       //      paragraph.append(data_user[index]);
       //      // paragraph.append(breakline);
       //      box_dashed.append(paragraph);
       // }
       //      // box_dashed.append(breakline);
       //

       // Just another way to do the same things in screen
       // If there aren't falses in return from validates,
       // the script will go ahead with the values
       box_dashed.style.display = "block";
       var p_name = document.querySelector("#p_name span");
       var p_surname = document.querySelector("#p_surname span");
       var p_age = document.querySelector("#p_age span");
       p_name.innerHTML = name;
       p_surname.innerHTML = surname;
       p_age.innerHTML = age;
       // document.append(document.createElement("hr"));
  });
});
