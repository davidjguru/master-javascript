"use strict"

function changeColor(color){
  element.style.background = color;
  return true;
}


// DOM - Document Object Model

var  element = document.getElementById("mybox");
console.log(element);

// Ok, but this lines will return a null value
// Because you're calling to this script before
// The index builds the <body> section.
// So, put the call inside the <body> after content.

// And now, we're getting the HTML
var html = document.getElementById("mybox").innerHTML;
console.log(html);

// Changing the text value
mybox.innerHTML = "AND NOW WE'RE CHANGING THE STRING";
element.innerHTML = "New output string";
// Modifying the style
element.style.background = "red";
element.style.padding = "20px";
element.style.color = "white";
// You can change all the properties
element.className = "hello";


// Use Query Selector
// Use nothing to take the tag name
var boxtest1 = document.querySelector("div");
console.log("Value by tag name: ", boxtest1);

// Use hashtag for select by id
var boxtest2 = document.querySelector("#mybox");
console.log("Value by id: ", boxtest2);

// Use dot to get the element by class name
var boxtest3 = document.querySelector(".hello");
console.log("Value by class: ", boxtest3);
