"use strict"

// And event is a function which is executed when something specific happens
// Warning: Now the clicks events called from index for button1 doesn't work.
// Now yout gotta to move it to EventListeners, instead of be called from HTML.
window.addEventListener('load', () => {
// Mouse Events
  var button1 = document.querySelector("#mybutton1");
  var button2 = document.querySelector("#mybutton2");
  var button3 = document.querySelector("#pushme");

  function changeColour(indexbutton){

    if(indexbutton == '#mybutton1'){
    console.log(button1);
    var bg1 = button1.style.background;
    console.log(bg1);
    if(bg1 == "green"){
    button1.style.background = "red";
    button1.style.padding = "35px";
    button1.style.border = "12px solid #000"

    }else{
     button1.style.background = "green";
     button1.style.padding = "10px";
     button1.style.border = "3px solid #ccc"
     }
   }else if (indexbutton == '#mybutton2') {
     console.log(button2);
     var bg2 = button2.style.background;
     console.log(bg2);
     if(bg2 == "yellow"){
     button2.style.background = "blue";
     button2.style.padding = "35px";
     button2.style.border = "12px solid #000"

     }else{
      button2.style.background = "yellow";
      button2.style.padding = "10px";
      button2.style.border = "3px solid #ccc"
      }
   }else if (indexbutton == '#pushme') {
     var bg3 = button3.style.background;
     console.log(button3);
     if(bg3 == "red"){
     button3.style.background = "black";
     }else if(bg3 == "black"){
     button3.style.background = "#ccc";
     }else{
     button3.style.background = "red";
     }

   }

     return true;
  }

  function launchBigButton(indexbutton) {
    if(indexbutton == '#mybutton1'){
    button1.style.background = "yellow";
    button1.style.padding = "100px";
    button1.style.border = "25px solid #000"
    console.log(button1.style.background);
  } else if (indexbutton == '#mybutton2') {
    button2.style.background = "orange";
    button2.style.padding = "100px";
    button2.style.border = "25px solid #000"
    console.log(button2.style.background);
  }
  return true;
  }

  // But we are mixing HTML and Javascript with our calls in the index.html
  // Ok, this is not a clean way, So we have to create an event listener.

    var pushme = document.querySelector("#pushme");
    pushme.addEventListener('click', function(){
          changeColour('#pushme');
          // Use of this to reference the element (button pushme)
          this.style.border = "10px solid black";
     });

    // Click
    var button2 = document.querySelector("#mybutton2");
      // Gives an error: you can't call a function directly.
      // button2.addEventListener('click', cambiarColor());
    button2.addEventListener('click', function(){
        changeColour('#mybutton2');
      });

    // DoubleClick
    button2.addEventListener('dblclick', function(){
        launchBigButton('#mybutton2');
      });
      // Now this way works fine -BUT- is changing the first button
      // We have to refactoring the two main functions with buttons,
      // for made it working with parameters. Let's go!

    // Mouse over
    button2.addEventListener('mouseover', function(){
    button2.style.background = "yellow";
    });

    // Mouse out
    button2.addEventListener('mouseout', function(){
    button2.style.background = "#ccc";
    });

   // Focus
    var inputname = document.querySelector("#name_field");
     console.log(inputname);
     inputname.addEventListener('focus', function(){
       console.log("Ok, you're inside the focus Event");
     });

   // Blur
   inputname.addEventListener('blur', function(){
     console.log("Ok, you're inside the Blur Event");
   });

   // Keydown
   inputname.addEventListener('keydown', function(){
     console.log("[keydown] - Ok, you're inside the KeyDown Event with key: ",
      String.fromCharCode(event.keyCode));
   });
   // Keypress
   inputname.addEventListener('keypress', function(){
     console.log("[keypress] - Ok, you're inside the KeyPress Event with key: ",
      String.fromCharCode(event.keyCode));
   });
   // Keyup
   inputname.addEventListener('keyup', function(){
     console.log("[keyup] - Ok, you're inside the KeyUp Event with key: ",
      String.fromCharCode(event.keyCode));
   });

  // Load - It's an even when the DOM is ready
  // See the new top of the script.
  // Now we can call it at the init of the index
 });
