"use strict"

// Event load
window.addEventListener('load', function(){
// Timers
  function interval(){
    var timing = setInterval(function(){
    console.log("Set Interval executed");
    var head = document.querySelector("h1");
    console.log(head);
    if (head.style.fontSize == "50px"){
      head.style.fontSize = "100px";
    }else{
      head.style.fontSize = "50px";
    }
  // End of setInterval
  }, 1000);
  return timing;
}

  var time = interval();
  var stop = document.querySelector("#stopbutton");
  stop.addEventListener("click", function(){
  alert("Clicked the stop button");
  clearInterval(time);
// End of eventListener click
});

  var start = document.querySelector("#startbutton");
  start.addEventListener("click", function(){
  alert("Clicked the start button");
  time = interval();
  // End of eventListener click
   });
// Exit from Load Event
});
