"use strict"

// Looking for all the divs from the index
// We know how getting elements by ids, ok.
// Now we're getting values by tag.

var allTheDivs = document.getElementsByTagName('div');
console.log(allTheDivs);

// Accessing to the values through the array
var textContentVar = allTheDivs[2];
console.log(textContentVar);
textContentVar.innerHTML = "Changing the string";
console.log(textContentVar);
textContentVar.style.background = "green";

var hr = document.createElement("hr");
console.log(hr);

console.log(allTheDivs);
var value;
for (value in allTheDivs) {
  if(typeof allTheDivs[value].textContent == 'string'){
  // console.log(value);
  var paragraph = document.createElement("p");
  var text = document.createTextNode(allTheDivs[value].textContent);
  paragraph.append(text);
  document.querySelector("#mysection").prepend(paragraph);

  }
}
document.querySelector("#mysection").append(hr);
document.querySelector("#mysection").prepend(hr);

// Getting elements by class
var redDivs = document.getElementsByClassName('red');
console.log("Array with values from the Red Divs: " +redDivs);
var divLoop;
for (divLoop in redDivs) {
  // Doesn't work, cause all the values are not only divs
  // redDivs[divLoop].style.background = "red";
  // So, what can I do?
  if(redDivs[divLoop].className == "red"){
    redDivs[divLoop].style.background = "red";
  }
// And now, everything is ok :-)
  console.log(redDivs[divLoop]);
}

// We will catch the elements marked with "yellow" class
var yellowDivs = document.getElementsByClassName("yellow");
console.log(yellowDivs);
yellowDivs[0].style.background = "yellow";

// And now we're going to take a chance with querySelector
var id = document.querySelector("#firstplace");
console.log(id);

var redclass = document.querySelector(".red");
console.log(redclass);
// Returns only one value, not a set of divs :-/
// Better use getElementsByClassName for more than a value?

// Sure? no, you can use this function: querySelectorAll
var elementList = document.querySelectorAll(".red");
console.log(elementList);
