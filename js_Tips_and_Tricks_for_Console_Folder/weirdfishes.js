let weirdFishes = [
  {
    common_name: "common fangtooth",
    scientific_name: "Anoplogaster cornuta",
    family: "Anoplogastridae",
    wikipedia_link: "https://en.wikipedia.org/wiki/Fangtooth"
  },
  {
    common_name: "vampire squid",
    scientific_name: "Vampyroteuthis infernalis",
    family: "Vampyroteuthidae",
    wikipedia_link: "https://en.wikipedia.org/wiki/Vampire_squid"
  },
  {
    common_name: "blobfish",
    scientific_name: "Psychrolutes marcidus",
    family: "Anoplogastridae",
    wikipedia_link: "https://en.wikipedia.org/wiki/Blobfish"
  },
  {
    common_name: "dumbo octopus",
    scientific_name: "Grimpoteuthis abyssicola",
    family: "Opisthoteuthidae",
    wikipedia_link: "https://en.wikipedia.org/wiki/Grimpoteuthis_abyssicola"
  }
];

console.table(weirdFishes);
