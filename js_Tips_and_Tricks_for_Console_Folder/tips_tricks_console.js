// The Console -in Firefox or Chrome - will be the main tool
// for working with Javascript (Chrome Dev Tools)

// We have to ensure all the document is loaded previously
window.onload = function() {

// Let's see several examples of the use of Console

// First: console.log()
// Shows a message within the console in browser
function reusableFunction(){
    console.log("Hi World: This is the first call to console.log");
}

reusableFunction();

// Second: console.table()
// Shows data in a table format, We're getting all the paragraphs
var paragraphs = document.getElementsByTagName("p");
console.table(paragraphs, "innerText");

// Third: console.clear()
// Clean up the console output
// Be aware:  in Google Chrome, console.clear() has no effect if the user has selected "Preserve log upon navigation" in the settings.
 // console.clear();

// Fourth: Using Alerts to get the screen size
// Getting the screen values Height and Width with Javascript
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
alert(x + ' × ' + y);
}

// Fifth: Applying styles to the console output
// Simple calling
console.log("%cWARNING", "color: red; font-size: 3rem;font-weight:bold;");
// Extended calling with more CSS
console.log(
        "%cPROTECT YA NECK!",
        "color:red;font-family:system-ui;font-size:4rem;-webkit-text-stroke: 1px black;font-weight:bold"
      );
// Styles and Colors
let colors = {
    "gray": "font-weight: bold; color: #1B2B34;",
    "red": "font-weight: bold; color: #EC5f67;",
    "orange": "font-weight: bold; color: #F99157;",
    "yellow": "font-weight: bold; color: #FAC863;",
    "green": "font-weight: bold; color: #99C794;",
    "teal": "font-weight: bold; color: #5FB3B3;",
    "blue": "font-weight: bold; color: #6699CC;",
    "purple": "font-weight: bold; color: #C594C5;",
    "brown": "font-weight: bold; color: #AB7967;"
      }
let backgrounds = {
     "black": "background: black; color: white; display: block;",
     "blue": "background: blue; color: white; display: block;",
     "red": "background: red; color: white; display: block;",
     "yellow": "background: yellow; color: white; display: block;",
     "green": "background: green; color: white; display: block;",
     "pink": "background: pink; color: white; display: block;"
}
// All the colors
  console.log('%cHello One', colors.gray);
  console.log('%cHello Two', colors.red);
  console.log('%cHello Three', colors.orange);
  console.log('%cHello Four', colors.yellow);
  console.log('%cHello Five', colors.teal);
  console.log('%cHello Six', colors.blue);
  console.log('%cHello Seven', colors.purple);
  console.log('%cHello Eight', colors.brown);

// All the backgrounds
  console.log('%cThis is the black background', backgrounds.black);
  console.log('%cThis is the blue background', backgrounds.blue);
  console.log('%cThis is the red background', backgrounds.red);
  console.log('%cThis is the yellow background', backgrounds.yellow);
  console.log('%cThis is the green background', backgrounds.green);
  console.log('%cThis is the pink background', backgrounds.pink);

// Playing with the Style
  var styles = [
      'background: linear-gradient(#D33106, #571402)'
      , 'border: 1px solid #3E0E02'
      , 'color: white'
      , 'display: block'
      , 'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)'
      , 'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset'
      , 'line-height: 40px'
      , 'text-align: center'
      , 'font-weight: bold'
  ].join(';');

  console.log('%c This is just a special log message', styles);

// Using an external function to print in console
function myCustomLog(message, color="black") {
  switch (color) {
    case "info":
      color = "Green";
      fontsize = "2rem";
      break;
    case "success":
      color = "Blue";
      fontsize = "3rem";
      break;
    case "warning":
      color = "Orange";
      fontsize = "4rem";
      break;
    case "error":
      color = "Red";
      fontsize = "5rem";
      break;
    default:
      color = color;
      fontsize = "1rem";
     }

  console.log(`%c${message}`, `color:${color}; font-size:${fontsize};`);
}

myCustomLog("Calling to my custom log without a color value, using default");
myCustomLog("Now, a Success! message", "success");
myCustomLog("Gets an Error!", "error");
myCustomLog("This is a Warning!!", "warning");
myCustomLog("Take more Info...", "info");
