// Configure Slider
$(document).ready(function(){

  if(window.location.href.indexOf('index') > -1){
      $('.bxslider').bxSlider({
        mode: 'fade',
        captions: true,
        slideWidth: 1200,
        slideHeight: 100,
        infiniteLoop: true,
        adaptiveHeight: true
      });

    }

// Build Posts List
 if(window.location.href.indexOf('index') > -1){
     var posts = [
       {
         title: 'Test of Title 1',
         date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
         content: 'Lorem fistrum por la gloria de mi madre amatomaa voluptate aliqua la caidita nostrud enim quis al ataquerl mamaar. Sit amet occaecat sed amatomaa ut a wan.'
        },
       {
        title: 'Test of Title 2',
        date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
        content: 'Torpedo adipisicing condemor va usté muy cargadoo mamaar ese hombree ese hombree occaecat caballo blanco caballo negroorl officia nisi.'},
       {
         title: 'Test of Title 3',
         date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
         content: 'hasta luego Lucas caballo blanco caballo negroorl apetecan a gramenawer va usté muy cargadoo pecador.'
       },
       {
         title: 'Test of Title 4',
        date: 'Published the day: ' + moment().date() + ' of ' + moment().format("MMMM") + ', year ' + moment().format("YYYY"),
        content: 'Te voy a borrar el cerito hasta luego Lucas qué dise usteer me cago en tus muelas por la gloria de mi madre a gramenawer diodenoo sexuarl va usté muy cargadoo. Por la gloria de mi madre qué dise usteer llevame al sircoo sexuarl. No puedor ese pedazo de jarl caballo blanco caballo negroorl torpedo ahorarr diodeno benemeritaar.'
      }
     ];

     console.log(posts);
     posts.forEach((item, index) => {
        var post =
        `<article class="post">
          <h2>${item.title}</h2>
          <span class="date">${item.date}</span>
          <p>${item.content}</p>
          <a href="#" class="button-more">Read More...</a>
        </article>`;
        console.log(post);
        $("#posts").append(post);
     });
}
// Theme selector
 var theme = $("#theme");
 $("#to-green").click(function(){
   theme.attr("href", "css/green.css");
 })
 $("#to-blue").click(function(){
   theme.attr("href", "css/blue.css");
 })
 $("#to-red").click(function(){
   theme.attr("href", "css/red.css");
 });

// Up Button over scroll
$('.up').click(function(e){
  e.preventDefault();
  $('html, body').animate({
    scrollTop: 0
  }, 500);
  return false;
});

// Fake login with LocalStorage
$("#login form").submit(function(){
var form_name = $("#form_name").val();
  localStorage.setItem("form_name", form_name);
});

var form_name = localStorage.getItem("form_name");
if((form_name != null) && form_name != "undefined"){
  var about_paragraph = $("#about p");
  about_paragraph.html("<br /><strong>Welcome, " + form_name + "</strong><br />");
  about_paragraph.append("<a href='#' id='logout'>Logout</a>");
  $("#login").hide();

  $("#logout").click(function(){
    localStorage.clear();
    location.reload();
  });
}

// Load jQuery UI Accordion
if(window.location.href.indexOf('about') > -1){
    $("#accordify").accordion();
  }

// Creating a clock
if(window.location.href.indexOf('clock') > -1){

   setInterval(function(){
     var clock = moment().format("hh:mm:ss");
     $("#clock").html(clock);
   }, 1000);
  }

// Contact Form Validation
if(window.location.href.indexOf('contact') > -1){
  $("form input[name='date']").datepicker({
    dateFormat: 'dd-mm-yy'
  });
  $.validate({
    lang: 'en',
    errorMessagePosition: 'top',
    scrollToTopOnError: true
  });
}

});
