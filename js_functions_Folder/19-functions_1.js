"use strict"

// Functions
// A function is only a set of intructions

// First case
function calculator(){
  return "Hello, I'm a little calculator";
}

console.log(calculator());

// Second case
function sendmessage(){
  console.log("Second function");
}

// Invoking the function
sendmessage();
sendmessage();
sendmessage();

// Third case
function doreturn(){
  console.log("Hello there, I'm the third function");

  return "Well, I'm the return message by console on demand";
}

var result = doreturn();
console.log(result);

// Fourth case
function basicoperations(number1, number2, show = false){

  if (show == false){
    console.log(show);
    console.log(number1 + number2);
    console.log(number1 - number2);
    console.log(number1 * number2);
    console.log(number1 / number2);
    console.log("************************");
  }else{
    document.write((show)+"<br/>");
    document.write((number1 + number2)+"<br/>");
    document.write((number1 - number2)+"<br/>");
    document.write((number1 * number2)+"<br/>");
    document.write((number1 / number2)+"<br/>");
    document.write("************************<br/>");
  }

}
// Call to the function
basicoperations(2,2);
basicoperations(20, 10);
basicoperations(30, 4, true);

/*
for (var i = 1; i <= 10; i++) {
  console.log(i);
  basicoperations(i, 8);
}
*/

// Fifth example
function printbyConsole(number1, number2){
  console.log(number1 + number2);
  console.log(number1 - number2);
  console.log(number1 * number2);
  console.log(number1 / number2);
  console.log("************************");
}
function printbyDocument(number1, number2){
  document.write((number1 + number2)+"<br/>");
  document.write((number1 - number2)+"<br/>");
  document.write((number1 * number2)+"<br/>");
  document.write((number1 / number2)+"<br/>");
  document.write("************************<br/>");

}

function basicoperationsTwo(number1, number2, show = false){
  if (show == false){
    printbyConsole(number1, number2);
  }else{
    printbyDocument(number1, number2);
  }
 return true;
}

// Calling
basicoperationsTwo(5, 6, true);
