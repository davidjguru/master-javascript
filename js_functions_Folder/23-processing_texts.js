"use strict"

// Let's go to see the various functions of Javascript to process texts

// First: Transforming Texts
var number = 1234;
var text1 = "Welcome to this file";
var text2 = "Learning about strings in Javascript";

// Passing the number to String format
var data = number.toString();
console.log(data);
console.log(typeof(data));

// Changing lowercase to uppercase
data = text1.toUpperCase();
console.log(data);
// And back
data = text2.toLowerCase();
console.log(data);

// Calculate length of a text
var long = text1.length;
console.log("Length of the String Text 1 is: ", long);
var name1 = "";
console.log(name1.length);
var nameArray = ["Joe", "John", "Mary"];
console.log(nameArray.length);
// Gives an error
// var name2 = null;
// console.log(name2.length);
// var name3 = 2;
// console.log(name3.length);

// Concatenating texts
var totaltext = text1.concat(" "+text2);
console.log("First concat: ", totaltext);
totaltext = text2 +" "+ text1;
console.log("Second concat: ", totaltext);

// Searching words and terms in Strings
// Using indexOf
var result = text1.indexOf("file");
console.log(result);
// Using Search
var result2 = text1.search("file");
console.log(result2);
// Returns -1 If this doesn't exist
var result3 = text1.search("John Doe");
console.log(result3);
// Using match
var result4 = text2.match("in");
console.log(result4);
// Match with global Reg-Exp
var result5 = text2.match(/in/g);
console.log(result5);

// Substrings and charsets
var result6 = text1.substr(3, 5);
console.log(result6);
var result7 = text2.charAt(12);
console.log(result7);
// This return false
var result8 = text1.startsWith("to");
console.log(result8);
// And this return true
var result9 = text1.startsWith("Wel");
console.log(result9);
var result10 = text2.endsWith("script");
console.log(result10);
var result11 = text2.includes("about");
console.log(result11);

// Replacing and cutting Texts
var result12 = text2.replace("Javascript", "PHP");
console.log(result12);
var result13 = text1.slice(4, 6);
console.log(result13);
var result14 = text1.split();
console.log(result14);
var result15 = text1.split(" ");
console.log(result15);
// Delete white spaces
var text3 = "   Well this is a string with white spaces     ";
var result16 = text3.trim();
console.log(result16);
