"use strict"

// Parameters REST and SPREAD
// REST: Indeterminate number of parameters
// SPREAD:

// Case REST
// Build and array of diverse values
function listOfFruits(fruit1, fruit2, ...rest_of_fruits){
  console.log("Fruit Number One: ", fruit1);
  console.log("Fruit Number Two: ", fruit2);
  console.log("Rest of Fruits: ", rest_of_fruits);
}

listOfFruits("Orange", "Apple");
listOfFruits("Orange", "Apple", "Watermelon", "Coco", "Pear");

// Case SPREAD
// Splitting and array of values
var fruits = ["Orange", "Apple"];
listOfFruits(...fruits, "Watermelon", "Pear", "Coco");
