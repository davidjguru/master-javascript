"use strict"

// Scope of the variables

function helloWorld(text){
// This is a local variable inside the function
var hello_World = "Text within the function";
console.log(text);
// But, Can I invoke a variable created out of this function?
console.log((number.toString()), typeof(number.toString()));
console.log(hello_World);
}

var number = 12;
var text = "I'm a global variable";
helloWorld(text);

// This call throws an error
console.log(hello_World);
