"use strict"

// Anonymous functions
// Is a function whitout name

var film = function(name){
  return "The name of the film is: " +name;
}


// Callback
// Is only a function executed inside another function
// Passed like a parameter to a main function

function add(number1, number2){
  var result = number1 + number2;

  return result;
}

console.log(add(4,5));


// Third example
function addTwo(number1, number2, addAndShow, addByTwo){
  var addresult = number1 + number2;
  addAndShow(addresult);
  addByTwo(addresult);
  return addresult;
}

addTwo(4, 5, function(data){
  console.log("The add is: ", data);
}, function(data){
  console.log("The add by two is: ", (data*2));
});

addTwo(5, 7, data => {
  console.log("View the add result with arrow function: ", data);
}, data => {
  console.log("The double add value from arrow fuction example is: ", (data*2));
});
