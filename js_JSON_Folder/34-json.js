"use strict"

// JSON - Javascript Object Notation

// Basic example of a JSON object
window.addEventListener('load', function(){
 console.log("Loaded DOM!");
var film = {
    title: 'Avengers 2',
    year: 2015,
    country: 'USA'
}

var films = [
    {title: "Iron Man 3", year: 2013, country: 'USA'},
    film
];

// Getting values from the JSON Object
console.log("Film object-> ", film);
console.log("Film title->", film.title);
console.log("Films Array-> ", films);

// Procesing values
var film_box = document.querySelector("#films");
console.log(film_box);
var test_box = document.querySelector("#test");
console.log(test_box);
var index;
for(index in films) {
  var p = document.createElement("p");
  p.append(films[index].title);
  film_box.append(p);
}
});
