"use strict"

// Event load
window.addEventListener('load', function(){
var addform = document.querySelector("#filmform");
addform.addEventListener('submit', function(){
console.log("We're within the submit function");
var title = document.querySelector('#addfilm').value;
if(title.length >= 1){
  localStorage.setItem(title, title);
}
});
var ul = document.querySelector('#filmlist');
for(var i in localStorage) {
  console.log(localStorage[i]);
  if(typeof localStorage[i] == 'string'){
  var li = document.createElement("li");
  li.append(localStorage[i]);
  ul.append(li);
  }
}

var deleteform = document.querySelector("#deleteform");
deleteform.addEventListener('submit', function(){
console.log("We're within the delete function");
var name = document.querySelector('#deletefilm').value;
if(name.length >= 1){
  localStorage.removeItem(name);
}
  });
});
