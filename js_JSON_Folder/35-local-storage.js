"use strict"
window.addEventListener('load', function(){
// Local storage
// We're going to store information within the web browser
// It's just like a memory for Frontend (like Sessions for Backend)

// Testing if there is a local storage in browser
if(typeof(Storage) !== 'undefined'){
  console.log("Local Storage is available :-)");
}else{
  console.log("Local Storage is not compatible :-( ");
}

// Saving data in Local Storage
localStorage.setItem("title", "testing Local Storage 2000");

// Getting an element from Local Storage
// First: Loaded by code
console.log(localStorage.getItem("title"));
// Second: Loaded by prompt in browser
console.log(localStorage.getItem("title2"));
// And everything It's ok :-)

// Setting elements in screen through DOM
document.querySelector("#films").innerHTML = localStorage.getItem("title");

// Saving JSON Objects in Local Storage
var user = {
    name: "David Rodríguez",
    email: "davidjguru@gmail.com",
    web: "davidjguru.github.io"
};

 // This Doesn't work fine: set an [object Object] in Storage
localStorage.setItem("user", user);
// Solution: cast to String
localStorage.setItem("user", JSON.stringify(user));

// Now, we want to get the JSON Object (string) from Storage
console.log(localStorage.getItem("user"));
// Ummmm, returns a string without fields :-/
// So, what? recast from String to JSON
var userjs = JSON.parse(localStorage.getItem("user"));
console.log(userjs);
document.querySelector("#test").append("User Name: "+userjs.name);

// Delete elements in Local Storage
// First: only one item
localStorage.removeItem("user");
// Second: all the values
localStorage.clear();
});
