"use strict"

// Arrays
// Data Collections

var name = "Only one value";
var names = ["John", "Mary", "James", "Kim", 43, true];
// Index ->    0        1       2       3     4    5
var languages = new Array("PHP", "JS", "Ruby", "Python", "C++");
console.log(names);
console.log(languages);
console.log(names[2]);
console.log(languages[3]);

// Length of an array
console.log(names.length);
console.log(languages.length);

// Index
var element = prompt("What item from the array is your favourite? ", 0);

if (element >= names.length){
  alert("Ok but the value is out of the index. Must be < "+names.length);
}else{
  alert(names[element]);
}

document.write("<h1>Programming languages from 2018</h1>");
document.write("<ul>");
for (var i = 0; i < languages.length; i++) {
  document.write("<li>" +languages[i]+"</li>" );
}
  document.write("</ul>");

// Going through an array with Callback
document.write("<ul>");
languages.forEach((element, index, data)=>{
  document.write("<li>Item number: "+index+" and element: "+element+"</li>")
});
document.write("</ul>");
