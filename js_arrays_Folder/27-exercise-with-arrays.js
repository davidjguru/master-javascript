"use strict"

/*
1. Ask six numbers by prompt and charge it in an array
2. Show the whole array (all the items) in the document body and console
3. Put the array in order and show it
4. Reverse the order and show the values
5. Show how many elements does the array have?
6. Search a item introduced by the user and look for it and what is its index
*/

// 1- First, the original array
var comics = [];
// 1- Second, we define the loop for the six values
for (var i = 0; i < 6; i++) {
  //comics[i] = prompt("Set a new comic title: ");
  comics.push(prompt("Set a new comic title: "));
}

// 2- Show the whole array in console and the page body
console.log("This is the whole original Array: "+"\n");
console.log(comics);
console.log("\n"+"********************************"+"\n");
document.write("<h2>This is the whole original Array:</h2> "+"<br/>")
document.write(comics);
document.write("<br>"+"********************************"+"</br>");

// 2 - Showing all the items with order and breaklines
console.log("This is the whole original Array item by item: "+"\n");
document.write("<h2>This is the whole original Array item by item:</h2> "+"<br/>");
for (var i = 0; i < comics.length; i++) {
  console.log(comics[i]+"\n");
  document.write(("<br/>"+comics[i]+"<br/>"));
}
console.log("\n"+"********************************"+"\n");
document.write("<br>"+"********************************"+"</br>");

// 3- Gives order to the comics Array
comics.sort();
console.log("This is the Array, now ordered: "+"\n");
document.write("<h2>This is the Array, now ordered:</h2> "+"<br/>");
for (var i = 0; i < comics.length; i++) {
  console.log(comics[i]+"\n");
  document.write(("<br/>"+comics[i]+"<br/>"));
}
console.log("\n"+"********************************"+"\n");
document.write("<br>"+"********************************"+"</br>");


// 4- Change the order and show the values
comics.reverse();
console.log("This is the Array, changed order: "+"\n");
document.write("<h2>This is the Array, changed order:</h2> "+"<br/>");
for (var i = 0; i < comics.length; i++) {
  console.log(comics[i]+"\n");
  document.write(("<br/>"+comics[i]+"<br/>"));
}
console.log("\n"+"********************************"+"\n");
document.write("<br>"+"********************************"+"</br>");

// 5- Show how many elements
console.log("Length of the Array: "+comics.length+"\n");
document.write("Length of the Array: "+comics.length+"<br/>");

// 6- Looking for a item and its index
var value = prompt("Ok, set the value to search: ");
// Return Index value
var index1 = comics.findIndex(comic => comic == value);
if(index1 != -1){
  // Get the item
  var item1 = comics.find(comic => comic == value);
  console.log("The value: "+item1+" Is in the position: "+index1);
}else{
  console.log("The value is not in the array");
}
