"use strict"

var categories = ['Action', 'History', 'Terror', 'Comedy', 'Sci-Fi'];
var films = ['Star Wars', 'Gran Torino', 'Rambo', 'Hangover', 'Dracula'];

var film = [categories, films];
console.log(film);

console.log(film[0][1]);
console.log(film[1][2]);

// Gives an error because you aren't in PHP, Dude
// films[] = "Batman";
// console.log(films);

// Another error, man
// films.add("Rocky");
// console.log(films);

films.push("Batman");
console.log(films);

var element = "";

do{
   element = prompt("Ok, set a new film to the list: ");
   films.push(element);
}while(element != "END");

console.log(films);

// Delete the last item
films.pop();
console.log(films);

// Delete an specific Item
// First, locate the item
var index = films.indexOf("Rambo");
console.log(index);
// Then, confirm if exist
if(index > -1){
  // Deleting the item
  films.splice(index, 1);
}
// From Array to String
console.log(films);
var filmString = films.join();
console.log(filmString);

// From String to Array
var string_basic = "text1, text2, text3";
var string_array = string_basic.split(", ");
console.log(string_array);

// Ordering an Array
console.log(films);
console.log(films.sort());
console.log(films.reverse());

for (let cat in categories) {
  document.write(categories[cat]+"<br/>");
}

// Searching in Arrays
var lookfor = categories.find(function(category){
  return category == "Comedy";
});
console.log(lookfor);

// Short version
var lookfor2 = categories.find(category => category == "Terror");
console.log(lookfor2);

// Return Index value
var index_value = categories.findIndex(category => category == "History");
console.log(index_value);

// Looking for values with a Anonymous Function
var prices = [10, 20, 30, 40, 50, 80, 12, 34];
var prices_result = prices.some(price => price >= 80);
console.log(prices_result);
