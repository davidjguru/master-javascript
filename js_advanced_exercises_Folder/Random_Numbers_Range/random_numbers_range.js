"use strict"

window.addEventListener('load', function(){

// Initial Testing Values and basic methods
console.log("The Random Numbers Script has been loaded");
var valueTest = Math.random();
console.log("Testing an initial call to Math.random: ", valueTest);
var valueRounded = Math.floor(valueTest * 20);
console.log("And now, we get the rounded value by floor: ", valueRounded);


// Only change code below this line.
function randomRange(myMin, myMax) {
  var randomValue = Math.floor((Math.random() * (myMax - myMin)) + myMin);
  return randomValue; // Change this line
}

// Change these values to test your function
console.log(randomRange(5, 15));
console.log(randomRange(2, 2));
console.log(randomRange(3, 10));
console.log(randomRange(1, 1));
console.log(randomRange(100, 900));


});
