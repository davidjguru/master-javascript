"use strict"

window.addEventListener('load', function(){
  console.log("The document has been loaded");

  var contacts = [
      {
          "firstName": "Akira",
          "lastName": "Laine",
          "number": "0543236543",
          "likes": ["Pizza", "Coding", "Brownie Points"]
      },
      {
          "firstName": "Harry",
          "lastName": "Potter",
          "number": "0994372684",
          "likes": ["Hogwarts", "Magic", "Hagrid"]
      },
      {
          "firstName": "Sherlock",
          "lastName": "Holmes",
          "number": "0487345643",
          "likes": ["Intriguing Cases", "Violin"]
      },
      {
          "firstName": "Kristian",
          "lastName": "Vos",
          "number": "unknown",
          "likes": ["JavaScript", "Gaming", "Foxes"]
      }
  ];


// Procesing values
var contacts_box = document.querySelector("#profiles-info");
console.log("This is the original div Profiles_Box: ", {contacts_box});
console.log(contacts_box);
var index;
for(index in contacts) {
  var p = document.createElement("p");
  p.append(JSON.stringify(contacts[index]));
  contacts_box.append(p);
}

function lookUpProfile(name, prop){
// Only change code below this line
 for (var i = 0; i < contacts.length; i++){
   if(contacts[i]['firstName'] === name){
      if(contacts[i].hasOwnProperty(prop)){
       return contacts[i][prop];
     }else {
       return "No such property";
     }
    }
   }
   return "No such contact";
}

// Change these values to test your function
console.log(lookUpProfile("Akira", "likes"));
console.log(lookUpProfile("Kristian", "lastName"));
console.log(lookUpProfile("Bob", "number"));
console.log(lookUpProfile("Akira", "adress"));
});
