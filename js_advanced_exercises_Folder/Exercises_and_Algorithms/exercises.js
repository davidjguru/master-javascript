"use strict"

window.addEventListener('load', function(){
  console.log("The document has been loaded");

// EXERCISE ONE: Get the sum of a set of numbers in a array
function sumAll(arr){
  let sum = 0;
  for(let i = Math.min(...arr); i <= Math.max(...arr); i++){
    sum += i;
  }
  return sum;
}

// console.log(sumAll([1,4]));
// console.log(sumAll([4,1]));
// console.log(sumAll([5,10]));}


// EXERCISE TWO: Diff Two Arrays
// MODEL 1
function diffArray(arr1, arr2) {
  let newArr = [];

  function lookByLoop(first, second){
    for(let i = 0; i < first.length; i++){
    if(!second.includes(first[i])){
      newArr.push(first[i]);
    }
  }
}

lookByLoop(arr1, arr2);
lookByLoop(arr2, arr1);
  return newArr;
}

// console.log(diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]))
// console.log(diffArray(["diorite", "andesite", "grass", "dirt", "pink wool", "dead shrub"], ["diorite", "andesite", "grass", "dirt", "dead shrub"]));
// console.log(diffArray([1, "calf", 3, "piglet"], [1, "calf", 3, 4]));
// console.log(diffArray([1, "calf", 3, "piglet"], [7, "filly"]));


// MODEL TWO
function diffArray2(arr1, arr2){
  return arr1
    .concat(arr2)
    .filter(
      item => !arr1.includes(item) || !arr2.includes(item)
    );
}

// console.log("----------------------------------------------");
// console.log(diffArray2([1, 2, 3, 5], [1, 2, 3, 4, 5]))
// console.log(diffArray2(["diorite", "andesite", "grass", "dirt", "pink wool", "dead shrub"], ["diorite", "andesite", "grass", "dirt", "dead shrub"]));
// console.log(diffArray2([1, "calf", 3, "piglet"], [1, "calf", 3, 4]));
// console.log(diffArray2([1, "calf", 3, "piglet"], [7, "filly"]));

// EXERCISE THREE: SEEK AND DESTROY
function destroyer(arr) {
  var args = Array.prototype.slice.call(arguments);
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < args.length; j++) {
      if (arr[i] === args[j]) {
        delete arr[i];
      }
    }
  }
  return arr.filter(Boolean);
}

//console.log(destroyer([1, 2, 3, 1, 2, 3], 2, 3));
//console.log(destroyer(["tree", "hamburger", 53], "tree", 53));
//console.log(destroyer(["possum", "trollo", 12, "safari", "hotdog", 92, 65, "grandma", "bugati", "trojan", "yacht"], "yacht", "possum", "trollo", "safari", "hotdog", "grandma", "bugati", "trojan"));
// console.log(destroyer([3, 5, 1, 2, 2], 2, 3, 5));

// EXERCISE FOUR:  WHEREFORE ART THOU
function whatIsInAName(collection, source) {
 let keys = Object.keys(source);
 return collection. filter(function(obj){
  for(let i = 0; i < keys.length; i++){
      if(!obj.hasOwnProperty(keys[i]) || obj[keys[i]] !== source[keys[i]]) {
        return false;
      }
    }
      return true;
    });


  }


// console.log(whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" }));
// console.log(whatIsInAName([{ "apple": 1 }, { "apple": 1 }, { "apple": 1, "bat": 2 }], { "apple": 1 }));
// console.log(whatIsInAName([{ "apple": 1, "bat": 2 }, { "bat": 2 }, { "apple": 1, "bat": 2, "cookie": 2 }], { "apple": 1, "bat": 2 }));
// console.log(whatIsInAName([{"a": 1, "b": 2, "c": 3}], {"a": 1, "b": 9999, "c": 3}));


// EXERCISE FIVE: SPINAL TAP CASE
function spinalCase(str) {
    let regex = /\s+|_+/g;
    let testr = str.replace(/([a-z])([A-Z])/g, '$1 $2');
    return ((testr.replace(regex, '-')).toLowerCase());
}


// console.log(spinalCase("This Is Spinal Tap"));
// console.log(spinalCase("thisIsSpinalTap"));
// console.log(spinalCase("The_Andy_Griffith_Show"));

// EXERCISE SIX: PIG LATIN
function translatePigLatin(str) {
  let finalString = '';
  let regex = /[aeiou]/gi;
  if(str[0].match(regex)) {
      finalString = str+'way';
  }
  else if(str.match(regex) === null) {
    finalString = str+'ay';
  }else{
    let numberOfVowels = str.indexOf(str.match(regex)[0]);
    finalString = str.substr(numberOfVowels) + str.substr(0, numberOfVowels) + 'ay';
  }

  return finalString;
}


// console.log(translatePigLatin("california"));
// console.log(translatePigLatin("algorithm"));
// console.log(translatePigLatin("glove"));

// EXERCISE SEVEN: SEARCH AND REPLACE
function myReplace(str, before, after) {
    let position = str.indexOf(before);
    if(str[position] === str[position].toUpperCase()) {
      after = after.charAt(0).toUpperCase() + after.slice(1);
    }
    str = str.replace(before, after);
  return str;
}


// console.log(myReplace("Let us go to the store", "store", "mall"));
// console.log(myReplace("His name is Tom", "Tom", "john"));
// console.log(myReplace("Let us get back to more Coding", "Coding", "algorithms"));

// EXERCISE EIGHT: DNA pairing
function pairElement(str) {
  let finalArray = [];
  let search = function(char) {
        switch (char) {
          case 'A':
            finalArray.push(['A', 'T']);
            break;
          case 'T':
            finalArray.push(['T', 'A']);
            break;
          case 'C':
            finalArray.push(['C', 'G']);
            break;
          case 'G':
            finalArray.push(['G', 'C']);
            break;
        }
      };
  for(let i = 0; i < str.length; i++){
      search(str[i]);

  }
  return finalArray;
}

console.log(pairElement("AC"));
// console.log(pairElement("GCG"));
// console.log(pairElement("ATCGA"));
// console.log(pairElement("CTCTA"));

});
