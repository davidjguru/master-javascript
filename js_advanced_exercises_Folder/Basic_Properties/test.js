"use strict"

window.addEventListener('load', function(){
  console.log("The document has been loaded");

  let users = {
    Alan: {
      age: 27,
      online: false
    },
    Jeff: {
      age: 32,
      online: true
    },
    Sarah: {
      age: 48,
      online: false
    },
    Ryan: {
      age: 19,
      online: true
    }
  };

// Counting elements with specific value in a property
  function countOnline(obj) {
// change code below this line
let counter = 0;
for (let user in obj){
if(obj[user].online == true){
  counter++;
}
}
return counter;
// change code above this line
}

// Reverse String with a loop
function reverseString(str) {
let slides = str.split("");
let varchar = "";
for(let i = ((slides.length)-1); i >= 0; i--){
    varchar += slides[i];
}
  return varchar;
}
reverseString("hello");

// Factorialize a number
function factorialize(num) {
 if (num === 0){return 1;}
 return num * factorialize(num - 1);
}

// Find the longest Word in a String
function findLongestWordLength(str) {
  let arrWords = str.split(" ");
  let maxSize = "";
  arrWords.forEach((value) => {
  if (value.length > maxSize.length){
    maxSize = value;
    }
  });
  return maxSize.length;
}

// Return Largest Numbers in Arrays
function largestOfFour(arr) {
let arrayReturn = new Array(4);
let maxValue = arr[0][0];

for(let i = 0; i < 4; i++){
  maxValue = arr[i][0];
  for(let j = 0; j < 4; j++){
    if((arr[i][j] > maxValue) || (arr[i][j] == maxValue)){
      maxValue = arr[i][j];
      arrayReturn[i] = maxValue;
    }
  }
}
return arrayReturn;
}

// Confirm the Ending
function confirmEnding(str, target) {
let cutString = str.slice(-(target.length));
if (cutString == target){
  return true;
}else{
  return false;
}
}

// Repeat a String, repeat a String
function repeatStringNumTimes(str, num) {
 let returnString = "";
 if (num > 0){
  for(let i = 0; i < num; i++){
    returnString += str;
  }    return returnString;
 }
  return returnString;
}

// Truncate a String
function truncateString(str, num) {
if((num => str.length)){
  return str;
}else{
  return (str.slice(0, num)) + "...";
}
}

//Finders Keepers
function findElement(arr, func) {
  let num = 0;
   for(let i = 0; i < arr.length; i++){
     num = arr[i];
     if(func(num)){
      return num;
     }
   }
   return undefined;
}

// Boo Who
function booWho(bool) {
  return (typeof bool === "boolean");
}

// Title Case a Sentence
function titleCase(str) {
  let wordsArray = str.toLowerCase().split(" ");
  let finalString = wordsArray.map(function(val){
    return val.replace(val.charAt(0), val.charAt(0).toUpperCase());
  });
  return finalString.join(" ");
}

// Slice and Splice
// You are given two arrays and an index.Use the array methods slice and splice
// to copy each element of the first array into the second array, in order.
// Begin inserting elements at index n of the second array.

// Return the resulting array. The input arrays should remain the same after
// the function runs.

function frankenSplice(arr1, arr2, n) {
  // It's alive. It's alive!
    let arr3 = [...arr2];
    for(let i = 0; i < arr1.length; i++){
      arr3.splice((n + i), 0, arr1[i]);
    }

  return arr3;
}

// Falsy Bouncer
// Remove all falsy values from an array.
// Falsy values in JavaScript are false, null, 0, "", undefined, and NaN.
// Hint: Try converting each value to a Boolean.

function bouncer_v1(arr) {
  // Don't show a false ID to this bouncer.
  let arrExtra = [];
  for(let i = 0; i < arr.length; i++){
    let value = new Boolean(arr[i]);
    if(value.valueOf() === true){
      arrExtra.splice((arrExtra.length), 0, arr[i]);
    }
  }
  return arrExtra;
}

function bouncer_v2(arr) {
    return arr.filter(Boolean);
}

// Where do I Belong
// Return the lowest index at which a value (second argument) should be
// inserted into an array (first argument) once it has been sorted.
// The returned value should be a number.
function getIndexToIns_v1(arr, num) {
  // Find my place in this sorted array.
  let position = 0;
  arr.sort((a, b) => a - b );
  for(let i = 0; i < arr.length; i++){
    if((((arr[i] < num) && (arr[i+1] >= num)) === true) || ((arr[i] <= num) && (i == (arr.length - 1)))){
      position = i+1;
    }
  }

  return position;
}

function getIndexToIns_v2(arr, num) {
  arr.sort(function(a, b) {
    return a - b;
  });

  for (var a = 0; a < arr.length; a++) {
    if (arr[a] >= num)
      return a;
  }

  return arr.length;
}

// Mutation
// Return true if the string in the first element of the array contains all of
// the letters of the string in the second element of the array.
function mutation(arr) {
  for(let i = 0; i < arr[1].length; i++){
      if((arr[0].indexOf(arr[1].charAt(i).toUpperCase())) === -1 && (arr[0].indexOf(arr[1].charAt(i).toLowerCase())) === -1){
        return false;
      }
    }

  return true;
}

// Chunky Monkey
// Write a function that splits an array (first argument) into groups the
// length of size (second argument) and returns them as a two-dimensional array.
function chunkArrayInGroups(arr, size) {
  // Break it up.
  let resultArray = [];
  for(let i = 0; i < arr.length; i+=size){
    resultArray.push(arr.slice(i, i+size));
  }

  return resultArray;
}


// console.log("CountOnLine", countOnline(users));

// console.log("Factorialize", factorialize(5));
// console.log("Factorialize", factorialize(0));

// console.log("Longest Word: ", findLongestWordLength("The quick brown fox jumped over the lazy dog"));

// console.log("Largest of Four: ", largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]));
// console.log("Largest of Four - two: ", largestOfFour([[17, 23, 25, 12], [25, 7, 34, 48], [4, -10, 18, 21], [-72, -3, -17, -10]]));

// console.log(confirmEnding("Bastian", "n"));
// console.log(confirmEnding("Congratulation", "on"));
// console.log(confirmEnding("Connor", "n"));

 // console.log(repeatStringNumTimes("abc", 3));
 // console.log(repeatStringNumTimes("*", 4));

// console.log(truncateString("A-tisket a-tasket A green and yellow basket", 8));
// console.log(truncateString("Peter Piper picked a peck of pickled peppers", 11));
// console.log(truncateString("A-tisket a-tasket A green and yellow basket", "A-tisket a-tasket A green and yellow basket".length));
// console.log(truncateString("A-tisket a-tasket A green and yellow basket", "A-tisket a-tasket A green and yellow basket".length + 2));
// console.log(truncateString("A-", 1));

// console.log(booWho(null));
// console.log(booWho([1, 2, 3]));
// console.log(booWho(false));

// console.log(titleCase("I'm a little tea pot"));
// console.log(titleCase("sHoRt AnD sToUt"));
// console.log(titleCase("HERE IS MY HANDLE HERE IS MY SPOUT"));

// console.log(frankenSplice([1, 2, 3], [4, 5, 6], 1));
// console.log(frankenSplice([1, 2], ["a", "b"], 1));
// console.log(frankenSplice(["claw", "tentacle"], ["head", "shoulders", "knees", "toes"], 2));

// console.log(bouncer_v1([7, "ate", "", false, 9]));
// console.log(bouncer_v1(["a", "b", "c"]));
// console.log(bouncer_v1([false, null, 0, NaN, undefined, ""]));

// console.log(bouncer_v2([7, "ate", "", false, 9]));
// console.log(bouncer_v2(["a", "b", "c"]));
// console.log(bouncer_v2([false, null, 0, NaN, undefined, ""]));

// console.log(getIndexToIns_v1([40, 60], 50));
// console.log(getIndexToIns_v1([5, 3, 20, 3], 5));
// console.log(getIndexToIns_v1([], 1));
// console.log(getIndexToIns_v1([1,2,3,4], 1.5));
// console.log(getIndexToIns_v1([10, 20, 30, 40, 50], 30));

// console.log(getIndexToIns_v2([40, 60], 50));
// console.log(getIndexToIns_v2([5, 3, 20, 3], 5));
// console.log(getIndexToIns_v2([], 1));
// console.log(getIndexToIns_v2([1,2,3,4], 1.5));
// console.log(getIndexToIns_v2([10, 20, 30, 40, 50], 30));

// console.log(mutation(["hello", "hey"]));
// console.log(mutation(["zyxwvutsrqponmlkjihgfedcba", "qrstu"]));
// console.log(mutation(["floor", "for"]));
// console.log(mutation(["hello", "Hello"]));
// console.log(mutation(["Mary", "Aarmy"]));

console.log(chunkArrayInGroups(["a", "b", "c", "d"], 2));
console.log(chunkArrayInGroups([0, 1, 2, 3, 4, 5], 4));
console.log(chunkArrayInGroups([0, 1, 2, 3, 4, 5, 6, 7, 8], 2));



});
