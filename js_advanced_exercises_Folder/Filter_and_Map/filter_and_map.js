"use strict"

window.addEventListener('load', function(){
  console.log("The document has been loaded");

// EXERCISE ONE
// the global variable
var watchList = [
                 {
                   "Title": "Inception",
                   "Year": "2010",
                   "Rated": "PG-13",
                   "Released": "16 Jul 2010",
                   "Runtime": "148 min",
                   "Genre": "Action, Adventure, Crime",
                   "Director": "Christopher Nolan",
                   "Writer": "Christopher Nolan",
                   "Actors": "Leonardo DiCaprio, Joseph Gordon-Levitt, Ellen Page, Tom Hardy",
                   "Plot": "A thief, who steals corporate secrets through use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.",
                   "Language": "English, Japanese, French",
                   "Country": "USA, UK",
                   "Awards": "Won 4 Oscars. Another 143 wins & 198 nominations.",
                   "Poster": "http://ia.media-imdb.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg",
                   "Metascore": "74",
                   "imdbRating": "8.8",
                   "imdbVotes": "1,446,708",
                   "imdbID": "tt1375666",
                   "Type": "movie",
                   "Response": "True"
                },
                {
                   "Title": "Interstellar",
                   "Year": "2014",
                   "Rated": "PG-13",
                   "Released": "07 Nov 2014",
                   "Runtime": "169 min",
                   "Genre": "Adventure, Drama, Sci-Fi",
                   "Director": "Christopher Nolan",
                   "Writer": "Jonathan Nolan, Christopher Nolan",
                   "Actors": "Ellen Burstyn, Matthew McConaughey, Mackenzie Foy, John Lithgow",
                   "Plot": "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
                   "Language": "English",
                   "Country": "USA, UK",
                   "Awards": "Won 1 Oscar. Another 39 wins & 132 nominations.",
                   "Poster": "http://ia.media-imdb.com/images/M/MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1_SX300.jpg",
                   "Metascore": "74",
                   "imdbRating": "8.6",
                   "imdbVotes": "910,366",
                   "imdbID": "tt0816692",
                   "Type": "movie",
                   "Response": "True"
                },
                {
                   "Title": "The Dark Knight",
                   "Year": "2008",
                   "Rated": "PG-13",
                   "Released": "18 Jul 2008",
                   "Runtime": "152 min",
                   "Genre": "Action, Adventure, Crime",
                   "Director": "Christopher Nolan",
                   "Writer": "Jonathan Nolan (screenplay), Christopher Nolan (screenplay), Christopher Nolan (story), David S. Goyer (story), Bob Kane (characters)",
                   "Actors": "Christian Bale, Heath Ledger, Aaron Eckhart, Michael Caine",
                   "Plot": "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.",
                   "Language": "English, Mandarin",
                   "Country": "USA, UK",
                   "Awards": "Won 2 Oscars. Another 146 wins & 142 nominations.",
                   "Poster": "http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX300.jpg",
                   "Metascore": "82",
                   "imdbRating": "9.0",
                   "imdbVotes": "1,652,832",
                   "imdbID": "tt0468569",
                   "Type": "movie",
                   "Response": "True"
                },
                {
                   "Title": "Batman Begins",
                   "Year": "2005",
                   "Rated": "PG-13",
                   "Released": "15 Jun 2005",
                   "Runtime": "140 min",
                   "Genre": "Action, Adventure",
                   "Director": "Christopher Nolan",
                   "Writer": "Bob Kane (characters), David S. Goyer (story), Christopher Nolan (screenplay), David S. Goyer (screenplay)",
                   "Actors": "Christian Bale, Michael Caine, Liam Neeson, Katie Holmes",
                   "Plot": "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from the corruption that Scarecrow and the League of Shadows have cast upon it.",
                   "Language": "English, Urdu, Mandarin",
                   "Country": "USA, UK",
                   "Awards": "Nominated for 1 Oscar. Another 15 wins & 66 nominations.",
                   "Poster": "http://ia.media-imdb.com/images/M/MV5BNTM3OTc0MzM2OV5BMl5BanBnXkFtZTYwNzUwMTI3._V1_SX300.jpg",
                   "Metascore": "70",
                   "imdbRating": "8.3",
                   "imdbVotes": "972,584",
                   "imdbID": "tt0372784",
                   "Type": "movie",
                   "Response": "True"
                },
                {
                   "Title": "Avatar",
                   "Year": "2009",
                   "Rated": "PG-13",
                   "Released": "18 Dec 2009",
                   "Runtime": "162 min",
                   "Genre": "Action, Adventure, Fantasy",
                   "Director": "James Cameron",
                   "Writer": "James Cameron",
                   "Actors": "Sam Worthington, Zoe Saldana, Sigourney Weaver, Stephen Lang",
                   "Plot": "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
                   "Language": "English, Spanish",
                   "Country": "USA, UK",
                   "Awards": "Won 3 Oscars. Another 80 wins & 121 nominations.",
                   "Poster": "http://ia.media-imdb.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg",
                   "Metascore": "83",
                   "imdbRating": "7.9",
                   "imdbVotes": "876,575",
                   "imdbID": "tt0499549",
                   "Type": "movie",
                   "Response": "True"
                }
];

// Add your code below this line.
// First version of the filter: Created as an usual external function.
// At first time, we'll create a filter by Metascore value.
function isMetaScoreBiggerThan(film){
  console.log(film.Metascore);
  return (parseFloat(film.Metascore) >= 80);
}
let firstFilteredList = watchList.filter(isMetaScoreBiggerThan);

// Second version of the filter: Now, created with arrow functions.
// At second time, we'll create a filter by imdbRating value.
let secondFilteredList = (watchList.filter(film => (parseFloat(film.imdbRating)) >= 8.0))
.map(function(film){
  let newFilm = {};
  newFilm['title'] = film.Title;
  newFilm['rating'] = film.imdbRating;
  return newFilm;

});
// Now, we'll use map() to change values filtered

// Add your code above this line

console.log("First Filter: ", firstFilteredList);
console.log("Second Filter: ", secondFilteredList);


// EXERCISE TWO
// the global Array
var s = [23, 65, 98, 5];

Array.prototype.myFilter = function(callback){
  var newArray = [];
  // Add your code below this line
  this.forEach(function(x){
    if (callback(x) == true){
      newArray.push(x);
    }
  })
  // Add your code above this line
  return newArray;

};

var new_s = s.myFilter(function(item){
  return item % 2 === 1;
});

// EXERCISE THREE
// Using reduce()

// Add your code below this line

// Long Version
let averageRating1 = watchList.filter(film => film.Director === "Christopher Nolan");
console.log("1- STEP:", averageRating1);

let averageRating2 = averageRating1.map(film => Number(film.imdbRating));
console.log("2- STEP:", averageRating2);

function getValues(x1, x2){
return x1 + x2;
}

let averageRating3 = averageRating2.reduce(getValues);

console.log("3- STEP:", averageRating3);

let finalValue = averageRating3 / averageRating2.length;

console.log("FINAL VALUE: ", finalValue);



// Short Version
  var averageRating = watchList.filter(x => x.Director === "Christopher Nolan").map(x => Number(x.imdbRating)).reduce((x1, x2) => x1 + x2) / watchList.filter(x => x.Director === "Christopher Nolan").length;

 console.log("SHORT VERSION VALUE: ", averageRating);


// EXERCISE FOUR
// Alphabetical Order for an Array using sort()


  // Add your code below this line
  function alphabeticalOrder(arr) {
    return arr.sort();
  }
  // Add your code above this line


console.log("The ordered Array: ", alphabeticalOrder(["a", "d", "c", "a", "z", "g"]));

// Now with numbers in ascending order and using concat to create another
// new array, don't mutate the original array.
let globalArray = [5, 6, 3, 2, 9];
function nonMutatingSort(arr) {
  // Add your code below this line
return globalArray.concat([]).sort(function(a, b){
    return a - b;
  });
  // Add your code above this line
}


console.log("Final Ordered Array: ", nonMutatingSort(globalArray));

// EXERCISE FIVE
function checkPositive(arr) {
  // Add your code below this line
  let valueFinal = arr.every(function(valorsito) {
    if(valorsito > 0){
      return true;
    }
    else if(valorsito = 0){
      true;
    }
    else {
      return false;
    }
  });

  return valueFinal;

  // Add your code above this line
}

// Short VERSION
function checkerPositive(arr) {
let finalValue =  arr.every(function(valorcete) {
    return valorcete >= 0;
  });
return finalValue;
}
console.log("WHAT PLAN - CASE 1: ", checkPositive([1, 2, 3, -4, 5]));
console.log("WHAT PLAN - CASE 2: ", checkPositive([1, 2, 3, 4, 5]));
});
