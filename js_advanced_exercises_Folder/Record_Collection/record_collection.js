"use strict"

// JSON - Javascript Object Notation

// Basic example of a JSON object
window.addEventListener('load', function(){
// Setup
var collection = {
    "2548": {
      "album": "Slippery When Wet",
      "artist": "Bon Jovi",
      "tracks": [
        "Let It Rock",
        "You Give Love a Bad Name"
      ]
    },
    "2468": {
      "album": "1999",
      "artist": "Prince",
      "tracks": [
        "1999",
        "Little Red Corvette"
      ]
    },
    "1245": {
      "artist": "Robert Palmer",
      "tracks": [ ]
    },
    "5439": {
      "album": "ABBA Gold"
    }
};

// Procesing values
var records_box = document.querySelector("#records-info");
console.log("This is the original div Records_Box: ", {records_box});
console.log(records_box);
var index;
for(index in collection) {
  var p = document.createElement("p");
  p.append(JSON.stringify(collection[index]));
  records_box.append(p);
}

//  Testing Conversion Formats through Console
console.log("1- Collection Raw Format [object/Object]: " + collection);
console.log("2- Collection String Format [String/Array]: " + JSON.stringify(collection));
console.log("3- Collection Parsed Format [object/Object]: " + JSON.parse(JSON.stringify(collection)));
console.dir(collection);

// Keep a copy of the collection for tests
var collectionCopy = JSON.parse(JSON.stringify(collection));
var workingString = JSON.stringify(collectionCopy);

// Only change code below this line
function updateRecords(id, prop, value) {
  var message = "";
if (workingString.indexOf(id) != -1){
  message = "The id value is in the Collection.";
  console.log(message);
  if((collection[id].hasOwnProperty(prop) === true) && (prop != "tracks") && (value != "")){
    message += "And the property also is in the Collection. "
    console.log(message);
    collection[id][prop] = value;
  }else if((collection[id].hasOwnProperty(prop) === false) && (prop != "tracks") && (value != "")){
    collection[id][prop] = value;
    message += "The Collection has been updated with the new property and value. "
    console.log(message);
  }else if((collection[id].hasOwnProperty(prop) === false) && (prop == "tracks") && (value != "")){
    collection[id][prop] = [value];
  }else if((collection[id].hasOwnProperty(prop) === true) && (prop == "tracks") && (value != "")){
    collection[id][prop].push(value);
  }else if((collectionCopy[id].hasOwnProperty(prop) === true) && (value === null || value === '')){
    delete collectionCopy[id][prop];
  }
}
else{
  message = "The id value is not existing in the Collection";
  console.log(message);
}


return collection;
}

// Alter values below to test your code
// This first call doesn't update the values
console.log(updateRecords(5439, "album", "ABBA Gold"));
// This second call adds a new property "artist"
console.log(updateRecords(5439, "artist", "ABBAC"));
// This second call adds a new property "tracks"
console.log(updateRecords(5439, "tracks", "Take a Chance on Me"));
console.log(updateRecords(2548, "artist", ""));
console.log(updateRecords(1245, "tracks", "Addicted to Love"));
console.log(updateRecords(2468, "tracks", "Free"));
console.log(updateRecords(2548, "tracks", ""));
console.log(updateRecords(1245, "album", "Riptide"));
});
