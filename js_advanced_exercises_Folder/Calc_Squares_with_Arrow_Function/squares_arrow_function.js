"use strict";

window.addEventListener('load', function(){
  console.log("The document Squares with Arrow Function has been loaded");

  const realNumberArray = [4, 5.6, -9.8, 3.14, 42, 6, 8.34, -2];
  const squareList = (arr) => {

    // change code below this line
    const cleanArray = arr.filter( (x) => {
          return (Number.isInteger(x)) && (x > 0);
    });
    console.log(cleanArray);

    const squaredIntegers = cleanArray.map((x) => {
        return x*x;
    });
    // change code above this line
    return squaredIntegers;
  };

  // test your code
  const squaredIntegers = squareList(realNumberArray);
  console.log(squaredIntegers);

});
